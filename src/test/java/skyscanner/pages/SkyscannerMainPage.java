package skyscanner.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;
import static skyscanner.SkyscannerProperties.*;

/**
 * Created by zeburek on 16.07.2017.
 */
public class SkyscannerMainPage {
    private SelenideElement mHeaderLoginButton = $(".login");
    private SelenideElement mHeaderAccountButton = $(".account");

    private SelenideElement mHeaderProfilePriceAlertsButton = $("#manage-price-alerts");

    private SelenideElement mNativeLoginIframe = $("#widget-frame");
    private SelenideElement mEmailField = $(By.xpath("//*[@id=\"Email\"]"));
    private SelenideElement mPasswordField = $("#Password");
    private SelenideElement mSubmitEmailAndPassButton = $("#button-native-login");

    private SelenideElement mOriginCountrySelectionField = $("#js-origin-input");
    private SelenideElement mDestinationCountrySelectionField = $("#js-destination-input");
    private SelenideElement mSearchButton = $(".js-search-button");

    public void login(){
        mHeaderLoginButton.click();
        mNativeLoginIframe.shouldBe(Condition.visible);
        switchTo().innerFrame("widget-frame");
        mEmailField.val(EMAIL);
        mPasswordField.val(PASSWORD);
        mSubmitEmailAndPassButton.click();
        switchTo().defaultContent();
        mHeaderAccountButton.shouldBe(Condition.visible);
    }

    public SkyscannerFlightsSearchResultPage searchForFlights(){
        mOriginCountrySelectionField.click();
        mOriginCountrySelectionField.val(ORIGIN_COUNTRY);
        sleep(500);
        mOriginCountrySelectionField.pressEnter();

        mDestinationCountrySelectionField.click();
        mDestinationCountrySelectionField.val(DESTINATION_COUNTRY);
        sleep(500);
        mDestinationCountrySelectionField.pressEnter();

        mSearchButton.click();

        return page(SkyscannerFlightsSearchResultPage.class);
    }

    public SkyscannerAccountPage goToPriceAlertsPage(){
        mHeaderAccountButton.click();
        mHeaderProfilePriceAlertsButton.should(Condition.visible);
        mHeaderProfilePriceAlertsButton.click();

        return page(SkyscannerAccountPage.class);
    }
}

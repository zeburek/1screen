package skyscanner.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by zeburek on 16.07.2017.
 */
public class SkyscannerFlightsSearchResultPage {
    private SelenideElement mSearchSummaryPlacesHeader = $(By.xpath("//*[@class=\"search-summary-places \"]/h2"));
    private SelenideElement mPriceAlertsButton = $("#price-alert-button2");
    private SelenideElement mPriceAlertsButtonText = $(".price-alerts-message");


    public SelenideElement getSearchSummaryPlacesHeader(){
        return mSearchSummaryPlacesHeader;
    }

    public void addPriceAlerts(){
        mPriceAlertsButton.click();
        mPriceAlertsButtonText.shouldHave(Condition.exactText("Added to Price Alerts"));
    }
}

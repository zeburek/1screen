package skyscanner.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by zeburek on 16.07.2017.
 */
public class SkyscannerAccountPage {
    private SelenideElement mPriceAlertsPageFirstElementOriginCountry =
            $(By.xpath("//*[@class=\"price-alerts-list\"]/ul/li[1]/h4/em[1]"));
    private SelenideElement mPriceAlertsPageFirstElementDestinationCountry =
            $(By.xpath("//*[@class=\"price-alerts-list\"]/ul/li[1]/h4/em[2]"));

    public String getFirstOriginCountry(){
        mPriceAlertsPageFirstElementOriginCountry.should(Condition.visible);
        return mPriceAlertsPageFirstElementOriginCountry.val();
    }

    public String getFirstDestinationCountry(){
        mPriceAlertsPageFirstElementDestinationCountry.should(Condition.visible);
        return mPriceAlertsPageFirstElementDestinationCountry.val();
    }
}

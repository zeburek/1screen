package skyscanner;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.Assert;
import skyscanner.pages.SkyscannerAccountPage;
import skyscanner.pages.SkyscannerFlightsSearchResultPage;
import skyscanner.pages.SkyscannerMainPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import selenium.SeleniumProperties;

import static com.codeborne.selenide.Selenide.open;
import static skyscanner.SkyscannerProperties.DESTINATION_COUNTRY;
import static skyscanner.SkyscannerProperties.ORIGIN_COUNTRY;

/**
 * Created by zeburek on 16.07.2017.
 */
public class SkyscannerTest {
    @Before
    public void init(){
        SeleniumProperties.setDriverProperty();
        WebDriver driver = SeleniumProperties.getCustomWebDriver();
        WebDriverRunner.setWebDriver(driver);
        Configuration.browser = "chrome";
    }

    @Test
    public void resultCountriesEqualsSearchCountries() throws InterruptedException {
        SkyscannerMainPage skyscannerMainPage =
                open("https://www.skyscanner.ae", SkyscannerMainPage.class);
        skyscannerMainPage.login();

        SkyscannerFlightsSearchResultPage skyscannerResultPage = skyscannerMainPage.searchForFlights();
        boolean originCountrySaved =
                skyscannerResultPage.getSearchSummaryPlacesHeader().val().startsWith(ORIGIN_COUNTRY);
        Assert.assertTrue(originCountrySaved);
        boolean destinationCountrySaved =
                skyscannerResultPage.getSearchSummaryPlacesHeader().val().contains(DESTINATION_COUNTRY);
        Assert.assertTrue(destinationCountrySaved);

        SkyscannerAccountPage skyscannerAccountPage = skyscannerMainPage.goToPriceAlertsPage();
        String alertOriginCountry = skyscannerAccountPage.getFirstOriginCountry();
        String alertDestinationCountry = skyscannerAccountPage.getFirstDestinationCountry();

        Assert.assertEquals(ORIGIN_COUNTRY, alertOriginCountry);
        Assert.assertEquals(DESTINATION_COUNTRY, alertDestinationCountry);
    }

    @After
    public void end(){
        WebDriverRunner.closeWebDriver();
    }
}

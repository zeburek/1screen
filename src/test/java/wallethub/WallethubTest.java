package wallethub;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import selenium.SeleniumProperties;
import wallethub.pages.WallethubCompanyProfilePage;
import wallethub.pages.WallethubLoginPage;
import wallethub.pages.WallethubUserProfilePage;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;

/**
 * Created by zeburek on 05.07.2017.
 */
public class WallethubTest {
    @BeforeClass
    public static void init(){
        SeleniumProperties.setDriverProperty();
        WebDriver driver = SeleniumProperties.getCustomWebDriver();
        WebDriverRunner.setWebDriver(driver);
        Configuration.browser = "chrome";
    }

    @Test
    public void writeReviewAndCheckItInProfile() throws InterruptedException {
        WallethubLoginPage login = open("https://wallethub.com", WallethubLoginPage.class);
        login.goToLoginPage();
        login.authorize();
        login.waitForLogin();

        WallethubCompanyProfilePage page = open("http://wallethub.com/profile/test_insurance_company/",
                WallethubCompanyProfilePage.class);
        page.hoverRating();
        page.hoverStars();
        page.waitForReviewPage();
        page.selectPolicyReviewPage();
        page.writeReviewAndSubmit();

        WallethubUserProfilePage userProfile = page(WallethubUserProfilePage.class);
        userProfile.openProfile();
        Assert.assertTrue(userProfile.getWhPostedReview().exists());
    }

    @AfterClass
    public static void end(){
        WebDriverRunner.closeWebDriver();
    }
}

package wallethub.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.cssClass;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static wallethub.WallethubProperties.REVIEW_TEXT;

/**
 * Created by zeburek on 07.07.2017.
 */
public class WallethubCompanyProfilePage {
    private SelenideElement whRating = $(".wh-rating");
    private SelenideElement whRatingChoise = $(".wh-rating-choices");
    private ElementsCollection whRatingStars = $$(By.xpath("//*[@class=\"wh-rating-choices-holder\"]/a"));

    private SelenideElement whDropdownProductsMenu = $(".dropdown-list-new");
    private ElementsCollection whDropdownProducts = $$(By.xpath("//*[@class=\"drop-el\"]//a"));
    private SelenideElement whLoadingImage = $(".loading-image");
    private ElementsCollection whOverallRating = $$(By.xpath("//*[@class=\"rating-overall\"]//a"));
    private SelenideElement whReviewTextArea = $("#review-content");
    private SelenideElement whReviewTextAreaSubmit = $(By.xpath("//*[@class=\"submit\"]/input"));

    private SelenideElement whReviewSuccessText = $(By.xpath("//*[@class=\"content small\"]/p"));

    public void hoverRating(){
        whRating.shouldBe(Condition.visible);
        whRating.hover();
        whRating.shouldBe(Condition.disappear);
        whRatingChoise.shouldBe(Condition.visible);
    }

    public void hoverStars(){
        whRatingStars.get(4).hover();
        whRatingStars.get(3).shouldHave(cssClass("hover"));
        whRatingStars.get(3).click();
    }

    public void waitForReviewPage(){
        whDropdownProductsMenu.shouldBe(Condition.visible);
    }

    public void selectPolicyReviewPage() {
        whDropdownProductsMenu.click();
        whDropdownProducts.get(1).click();
        whLoadingImage.shouldNot(Condition.visible);
        whOverallRating.get(3).click();
    }

    public void writeReviewAndSubmit(){
        whReviewTextArea.val(REVIEW_TEXT);
        whReviewTextAreaSubmit.click();
        whReviewSuccessText.shouldBe(Condition.matchesText(REVIEW_TEXT));
    }
}

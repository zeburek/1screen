package wallethub.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static wallethub.WallethubProperties.LOGIN;
import static wallethub.WallethubProperties.PASS;

/**
 * Created by zeburek on 06.07.2017.
 */
public class WallethubLoginPage {
    private SelenideElement loginHeaderButton = $(".login");
    private SelenideElement emailField = $(By.name("em"));
    private SelenideElement passField = $(By.name("pw"));

    public void goToLoginPage(){
        loginHeaderButton.click();
    }

    public void authorize(){
        emailField.shouldBe(Condition.visible);
        emailField.val(LOGIN);
        passField.val(PASS);
        passField.pressEnter();
    }

    public void waitForLogin(){
        sleep(5000);
    }
}

package wallethub.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.sleep;
import static wallethub.WallethubProperties.REVIEW_TEXT;

/**
 * Created by zeburek on 07.07.2017.
 */
public class WallethubUserProfilePage {
    private SelenideElement whMenuHeader = $(".user");
    private ElementsCollection whUserMenu = $$(By.xpath("//*[@id=\"m-user\"]//a"));
    private SelenideElement whPostedReview = $(byText(REVIEW_TEXT));

    public void openProfile(){
        whMenuHeader.hover();
        sleep(1000);
        whUserMenu.get(3).shouldBe(Condition.visible);
        whUserMenu.get(3).click();
    }

    public SelenideElement getWhPostedReview(){
        return whPostedReview;
    }
}

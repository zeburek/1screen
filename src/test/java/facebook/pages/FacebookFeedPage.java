package facebook.pages;

import com.codeborne.selenide.SelenideElement;

import java.util.concurrent.TimeUnit;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static facebook.FacebookProperties.POST_STRING;

/**
 * Created by zeburek on 05.07.2017.
 */
public class FacebookFeedPage {
    private SelenideElement statusFieldFake = $("textarea");
    private SelenideElement statusField = $(".notranslate");
    private SelenideElement submitStatusButton = $("button.selected");

    public void enterTextToStatusField(String text) throws InterruptedException {
        statusFieldFake.click();
        TimeUnit.SECONDS.sleep(5);
        statusField.val(" ");
        statusField.val(text);
    }

    public void submitStatus() throws InterruptedException {
        submitStatusButton.click();
    }

    public SelenideElement getPostedElement(){
        return $(byText(POST_STRING));
    }
}
